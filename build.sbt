import com.typesafe.sbt.packager.docker._
import ReleaseTransformations._

name := """noodles"""
organization := "com.trawlinclusive"
maintainer := "contact@akeem.co"

version := s"""${(version in ThisBuild).value}"""
releaseVersionBump := sbtrelease.Version.Bump.Minor

lazy val root = (project in file(".")).enablePlugins(PlayScala, DockerPlugin)

packageName in Docker := packageName.value
daemonUser in Docker := "root"
dockerRepository := Some("cloud.canister.io:5000/akeem")
dockerExposedPorts := Seq(9000)
version in Docker := version.value

val DOCKER_UPDATE_LATEST = sys.env.get("DOCKER_UPDATE_LATEST").getOrElse("false")
dockerUpdateLatest := DOCKER_UPDATE_LATEST == "true"

publishTo := Some(Resolver.file("file", new File(Path.userHome.absolutePath + "/.m2/repository")))

dockerCommands := Seq(
  Cmd("FROM", "openjdk:8u191-alpine"),
  Cmd("LABEL", s"""MAINTAINER="${maintainer.value}""""),
  Cmd("WORKDIR", "/opt/docker"),
  Cmd("ADD", "--chown=root:root opt /opt"),
  Cmd("USER", "root"),
  Cmd("EXPOSE", "9000"),
  ExecCmd("RUN", "apk", "add", "--no-cache", "bash"),
  ExecCmd("ENTRYPOINT", "/opt/docker/bin/noodles"),
)

scalaVersion := "2.12.8"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.trawlinclusive.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.trawlinclusive.binders._"

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)

releaseProcess := Seq(
  checkSnapshotDependencies,
  inquireVersions,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  ReleaseStep(releaseStepTask(publish in Docker)),
  setNextVersion,
  pushChanges
)