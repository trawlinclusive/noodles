#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ ! -z "$1" ]
then
    export DOCKER_UPDATE_LATEST=true
else
    export DOCKER_UPDATE_LATEST=false
fi

cd $DIR/noodles
sbt 'release with-defaults'
git commit -am 'Updated version'
git push
git push --tags
